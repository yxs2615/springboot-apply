findGisChild
===
*@author : Mr.Yan
*@create : 2020/6/26
*@description : 根据表明获取数据

    SELECT 
        id,
        name,
        code,
        fcode,
        level
    FROM 
        #text(tblName)#
    WHERE
        isValid = 'Y' 
        @if(isNotEmpty(code)){
           AND  code = #code#
        @}
        @if(isNotEmpty(fcode)){
           AND  fcode = #fcode#
        @}
