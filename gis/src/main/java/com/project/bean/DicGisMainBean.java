package com.project.bean;

import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

/**
 * @author : Mr.Yan
 * @program : com.project.bean
 * @create : 2020/06/24
 * @description :
 */
@Data
@Table(name = "gis_main")
public class DicGisMainBean extends BaseBean{

    public String code;
    public String name;
    public String tableName;
}
