package com.project.bean;

import lombok.Data;

/**
 * @author : Mr.Yan
 * @program : com.project.bean
 * @create : 2020/06/26
 * @description :
 */
@Data
public class DicGisChildBean extends BaseBean{
    private String code;
    private String name;
    private String fcode;
    private String level;
}
