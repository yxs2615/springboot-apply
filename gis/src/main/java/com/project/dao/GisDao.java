package com.project.dao;

import com.project.bean.DicGisChildBean;
import org.beetl.sql.core.annotatoin.SqlResource;

import java.util.List;
import java.util.Map;

/**
 * @author : Mr.Yan
 * @program : com.project.dao
 * @create : 2020/06/26
 * @description :
 */
@SqlResource(value = "gisSql")
public interface GisDao {

    /**
      * @author : Mr.Yan
      * @create : 2020/6/26
      * @description : 根据表明获取数据
      */
    List<DicGisChildBean> findGisChild(Map<String,Object> param);
}
