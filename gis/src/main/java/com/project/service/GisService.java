package com.project.service;

import com.project.bean.DicGisChildBean;
import com.project.bean.DicGisMainBean;

import java.util.List;
import java.util.Map;

/**
 * @author : Mr.Yan
 * @program : com.project.service
 * @create : 2020/06/24
 * @description :
 */
public interface GisService extends BaseService<DicGisMainBean> {

    /**
      * @author : Mr.Yan
      * @create : 2020/6/26
      * @description : 根据表明获取子类
      */
    List<DicGisChildBean> findGisChild(Map<String, Object> param);

    /**
      * @author : Mr.Yan
      * @create : 2020/6/26
      * @description : 获取行政区划得json字符传
     */
    String findGisChildJson(String code);
}
