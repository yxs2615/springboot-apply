package com.project.serviceImpl;

import com.project.bean.DicGisChildBean;
import com.project.bean.DicGisMainBean;
import com.project.dao.GisDao;
import com.project.service.GisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author : Mr.Yan
 * @program : com.project.serviceImpl
 * @create : 2020/06/24
 * @description :
 */
@Slf4j
@Service
@Transactional
public class GisServiceImpl extends BaseServiceImpl<DicGisMainBean> implements GisService {

    @Autowired
    private GisDao gisDao;

    @Override
    public List<DicGisChildBean> findGisChild(Map<String, Object> param) {
        return gisDao.findGisChild(param);
    }

    @Override
    public String findGisChildJson(String code) {

        ClassPathResource classPathResource = null;
        classPathResource = new ClassPathResource("gis/" + code + ".json");
        EncodedResource encRes = new EncodedResource(classPathResource,"UTF-8");
        String content  = null;
        try {
            content = FileCopyUtils.copyToString(encRes.getReader());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
