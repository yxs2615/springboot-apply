package com.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr.Yan
 * @program : com.project
 * @create : 2020/07/23
 * @description : 新闻发布启动项 （Flow 官网 / 基金会 官网）
 */
@SpringBootApplication
public class NewsApplication {

    /**
     * @author : Mr.Yan
     * @create : 2020/7/23
     * @description : 新闻发布启动项
     */
    public static void main(String[] args) {
        SpringApplication.run(NewsApplication.class, args);
    }
}
