package com.project.config.interceptor;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
* @Description: 登录拦截器
* @Param:
* @return:
* @Author: Mr.Yan
* @Date: 2018/12/11
*/

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    /**
     * @author : Mr.Yan
     * @create : 2020/3/28
     * @description : 配置跨域问题
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .maxAge(3600);
    }
    @Bean
    public SecurityInterceptor getSecurityInterceptor() {
        return new SecurityInterceptor();
    }

    /**
     * @author : Mr.Yan
     * @create : 2020/7/23
     * @description : 将文件夹地址设置成网络路径
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/upload/**").addResourceLocations("file:E:/images/");
        registry.addResourceHandler("/upfile/**").addResourceLocations("file:E:/files/");
    }
    /**
     * 对地址进行拦截
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration addInterceptor = registry.addInterceptor(getSecurityInterceptor());
        // 拦截配置
        addInterceptor.addPathPatterns("/**");

        super.addInterceptors(registry);
    }
}
