package com.project.config.shiro;

import com.project.entity.system.user.UserInfoEntity;
import org.apache.shiro.SecurityUtils;

/**
 * @author : Mr.Yan
 * @program : com.project.config.shiro.utils
 * @create : 2020/06/03
 * @description : shiro utils
 */
public class ShiroUtils {
    // 获取登陆的信息
    public static UserInfoEntity getPrincipal () {
        return (UserInfoEntity) SecurityUtils.getSubject().getPrincipal();
    }
}
