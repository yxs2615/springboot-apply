package com.project.control.news.vo;

import com.project.bean.NewBean;
import lombok.Data;

/**
 * @author : Mr.Yan
 * @program : com.project.control.news.vo
 * @create : 2020/06/29
 * @description :
 */
@Data
public class NewBeanVo extends NewBean {

    private String TypeName;
}
