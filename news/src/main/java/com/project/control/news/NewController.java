package com.project.control.news;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.project.bean.DicTypeBean;
import com.project.bean.NewBean;
import com.project.control.BaseController;
import com.project.service.DicTypeService;
import com.project.service.NewsService;
import com.project.utils.layui.LayDatas;
import com.project.utils.layui.PageUtils;
import com.project.utils.result.ResultModel;
import com.project.utils.result.ResultTools;
import com.project.utils.token.CheckToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author : Mr.Yan
 * @program : com.project.control.news
 * @create : 2020/06/29
 * @description : 新闻发布
 */

@RestController
@RequestMapping("/newsController")
@CrossOrigin(origins = "*", methods = {GET, POST}, allowedHeaders = "*", maxAge = 3600)
public class NewController extends BaseController {

    @Autowired
    private NewsService newsService;

    @Autowired
    private DicTypeService dicTypeService;

    @PostMapping(value = "/publish")
    @CheckToken
    public ResultModel publish(NewBean newBean) {
        try {
            if(ObjectUtil.isNotNull(newBean.getId())){
                newBean.setSysUpdateTime(DateUtil.parse(DateUtil.now()));
                newsService.update_Selective_ByPK(newBean);
            }else{
                newBean.setSysCreateTime(DateUtil.parse(DateUtil.now()));
                int insert = newsService.insert(newBean);
                newBean.setId(insert);
            }
            return ResultTools.success(newBean);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultTools.fail();
        }
    }

    @GetMapping(value = "/list")
    @CheckToken
    public LayDatas list(PageUtils pageUtils) {
        return LayDatas.data(newsService.findList(pageUtils));
    }

    @DeleteMapping(value = "/delete")
    @CheckToken
    public ResultModel delete(Integer id) {
        try {
            NewBean newBean = new NewBean();
            newBean.setId(id);
            newBean.setIsDelete("Y");
            newBean.setSysUpdateTime(DateUtil.parse(DateUtil.now()));
            newsService.update_Selective_ByPK(newBean);
            return ResultTools.success();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultTools.fail(e.getMessage());
        }
    }

    @GetMapping(value = "type")
    @CheckToken
    public ResultModel type() {
        try {
            DicTypeBean dicTypeBean = new DicTypeBean();
            dicTypeBean.setIsValid("Y");
            List<DicTypeBean> listByWhere = dicTypeService.find_ListByWhere(dicTypeBean);
            return ResultTools.success(listByWhere);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultTools.fail();
        }
    }
}
