package com.project.control.user;

import cn.hutool.core.util.ObjectUtil;
import com.project.bean.system.TblRoleBean;
import com.project.bean.system.TblUserBean;
import com.project.config.shiro.ShiroUtils;
import com.project.control.BaseController;
import com.project.entity.system.user.UserInfoEntity;
import com.project.service.system.UserService;
import com.project.utils.result.ResultModel;
import com.project.utils.result.ResultTools;
import com.project.utils.token.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author : Mr.Yan
 * @program : com.project.control.user
 * @create : 2020/07/23
 * @description : 用户登录页面
 */
@Slf4j
@RestController
@RequestMapping(value = "/user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    /**
     * @author : Mr.Yan
     * @create : 2020/3/29
     * @description : 登陆接口返回 token
     */
    @PostMapping(value = "/login")
    @LoginToken
    public ResultModel login(@RequestBody TblUserBean tblUserBean) {
        log.info("登陆 [user/login] 接口");
        try {
            /** 登陆token 创建，为后期单点登陆进行加入，创建token值*/
            String token = JwtUtil.createJWT(tblUserBean.getUserName(), tblUserBean.getPassWord());
            JwtToken jwtToken = new JwtToken(token);
            Subject subject = SecurityUtils.getSubject();
            subject.login(jwtToken);
            UserInfoEntity userInfoEntity = ShiroUtils.getPrincipal();
            Set<String> roles = new LinkedHashSet<>();
            Set<TblRoleBean> tblRoleBeanSet = userInfoEntity.getRoles();
            for (TblRoleBean tblRoleBean : tblRoleBeanSet) {
                roles.add(tblRoleBean.getPermission());
            }
            Map<String,Object> resultMap = new HashMap<>();
            resultMap.put("token", token);
            resultMap.put("roles", roles);
            log.info("结束");
            return ResultTools.success(resultMap);
        } catch (UnknownAccountException e) {
            e.printStackTrace();
            log.error(String.format("结束：%s",e.getMessage()));
            return ResultTools.fail(e.getMessage());
        }
    }
    /**
     * @author : Mr.Yan
     * @create : 2020/7/23
     * @description : 刷新token
     */
    @GetMapping (value = "/tokenRefresh")
    @CheckToken(required = false)
    public ResultModel tokenRefresh() {
        UserInfoEntity userInfoEntity = ShiroUtils.getPrincipal();
        if (ObjectUtil.isNull(userInfoEntity)) {
            throw new CustomException("超时登录！");
        }
        String token = JwtUtil.createJWT(userInfoEntity.getUserName(), userInfoEntity.getPassWord());
        JwtToken jwtToken = new JwtToken(token);
        Subject subject = SecurityUtils.getSubject();
        subject.login(jwtToken);
        return ResultTools.success(token);
    }
}
