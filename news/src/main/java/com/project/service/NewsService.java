package com.project.service;

import com.project.bean.NewBean;
import com.project.control.news.vo.NewBeanVo;
import com.project.utils.layui.PageUtils;
import org.beetl.sql.core.engine.PageQuery;

/**
 * @author : Mr.Yan
 * @program : com.project.service
 * @create : 2020/06/29
 * @description : 新闻发布
 */
public interface NewsService extends BaseService<NewBean> {
    PageQuery<NewBeanVo> findList(PageUtils pageUtils);
}
