package com.project.service;

import com.project.bean.DicTypeBean;

/**
 * @author : Mr.Yan
 * @program : com.project.service
 * @create : 2020/06/29
 * @description :
 */
public interface DicTypeService extends BaseService<DicTypeBean> {
}
