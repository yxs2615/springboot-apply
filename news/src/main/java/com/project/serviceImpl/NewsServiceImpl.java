package com.project.serviceImpl;

import com.project.bean.NewBean;
import com.project.control.news.vo.NewBeanVo;
import com.project.dao.NewsDao;
import com.project.service.NewsService;
import com.project.utils.layui.PageUtils;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : Mr.Yan
 * @program : com.project.serviceImpl
 * @create : 2020/06/29
 * @description : 新闻发布
 */
@Slf4j
@Service
@Transactional
public class NewsServiceImpl extends BaseServiceImpl<NewBean> implements NewsService {

    @Autowired
    private NewsDao newsDao;

    @Override
    public PageQuery<NewBeanVo> findList(PageUtils pageUtils) {
        PageQuery<NewBeanVo> list = newsDao.findList(pageUtils);
        return list;
    }
}
