package com.project.serviceImpl;

import com.project.bean.DicTypeBean;
import com.project.service.DicTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : Mr.Yan
 * @program : com.project.serviceImpl
 * @create : 2020/06/29
 * @description :
 */
@Slf4j
@Service
@Transactional
public class DicTypeServiceImpl extends BaseServiceImpl<DicTypeBean> implements DicTypeService {
}
