package com.project.bean;

import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

/**
 * @author : Mr.Yan
 * @program : com.project.bean
 * @create : 2020/06/29
 * @description :
 */
@Data
@Table(name = "dic_type")
public class DicTypeBean {

    private Integer id;

    private String code;

    private String name;

    private String parentCode;

    private String isValid;
}
