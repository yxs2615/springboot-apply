package com.project.bean;

import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

/**
 * @author : Mr.Yan
 * @program : com.project.control.news
 * @create : 2020/06/29
 * @description : 新闻Bean
 */
@Data
@Table( name = "tbl_news")
public class NewBean extends BaseBean {

    public String name;

    public String type;

    public String html;

    public String content;

    /** 记录审核状态 pass(通过) reject (不通过) wating (待审核) */
    public String auditState;

    /** 不通过原因 */
    public String reject;


}
