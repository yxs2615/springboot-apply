package com.project.dao;

import com.project.control.news.vo.NewBeanVo;
import com.project.utils.layui.PageUtils;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;


/**
 * @author : Mr.Yan
 * @program : com.project.dao
 * @create : 2020/06/29
 * @description :
 */
@SqlResource("newsSql")
public interface NewsDao {
    PageQuery<NewBeanVo> findList(PageUtils pageUtils);
}
