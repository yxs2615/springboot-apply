findList
===
*@author : Mr.Yan
*@create : 2020/6/29
*@description : 分页查询新闻

    SELECT
    @pageTag(){
        N.*,
        (SELECT name FROM dic_type AS T WHERE T.code = N.Type) AS typeName 
    @}
    FROM 
    tbl_news AS N 
    
    WHERE N.isValid = 'Y' AND N.isDelete = 'N' 
    @if(isNotEmpty(name)){
        AND N.name like  #'%'+name+ '%'# 
    @}
    @if(isNotEmpty(type)){
        AND N.type = #type#
    @}
    
    order by N.sysCreateTime DESC
    
    
