package com.project.service;

import org.beetl.sql.core.engine.PageQuery;

import java.util.List;

/**
 * @Author : Yan
 * @Description :  my custom service interface , get pass beetlsql sqlManger interface create this class
 */
public interface BaseService<T> {
    /***
     * @Author : Yan
     * @Create : 2019/12/19
     * @Description :  find this table all date list
     */
    public List<T> find_All();

    /**
     * @Author : Yan
     * @Create : 2019/12/19
     * @Description :  find this table all data list size
     */
    public long find_All_Size();

    /**
     * @Author : Yan
     * @Create : 2019/12/19
     * @Description :  find this table data by primary key
     */
    public T find_ObjByPK(int pk);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :   find this table data by where params
     */
    public T find_ObjByWhere(Object params);

    /**
     * @Author : Yan
     * @Create : 2019/12/19
     * @Description :  find this table data by where params , this params best Map<String,Object> ; but this data list is disorder
     */
    public List<T> find_ListByWhere(Object params);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :  find this table data by where params , this params best Map<String,Object> ; but this data list is disorder and limit size
     */
    public List<T> find_ListByWhere(Object params, long start, long size);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :  find this table data by where params , this params best Map<String,Object> ; but this data list is order
     */
    public List<T> find_ListByWhere(Object params, String orderBy);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :  find this table data by where params , this params best Map<String,Object> ; but this data list is order and limit size
     */
    public List<T> find_ListByWhere(Object params, long start, long size, String orderBy);

    /**
     * @Author : Yan
     * @Create : 2019/12/19
     * @Description :  find this table data by where params size , this params best Map<String,Object>
     */
    public long find_ListByWhere_Size(Object params);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :  insert data into this table , include if field is null insert , return primary key
     */
    public int insert(T params);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :  insert data into this table , include if field is null no insert , return primary key
     */
    public int insert_Selective(Object params);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :  insert batch data list into this table , return insert success length
     */
    public int insert_Batch(List<T> list);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description : update data into this table by primary key, include if field is null update , return update size
     */
    public int update_ByPK(T params);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :  update data into this table by primary key, include if field is null no update , return update size
     */
    public int update_Selective_ByPK(Object params);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :  update batch data list into this table , if field is null update , return insert success length
     */
    public int update_Batch_ByPK(List<T> list);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :  update batch data list into this table , if field is null no update , return insert success length
     */
    public int update_BatchSelective_ByPK(List<?> list);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description : delete this table by primary key , return delete success length
     */
    public int delete_ByPK(Object pkValue);

    /**
     * @Author : Yan
     * @Create : 2019/12/21
     * @Description : delete this table by primary key Array , return delete success length
     */
    public int delete_ByPKArray(String[] PkArray);

    /**
     * @Author : Yan
     * @Create : 2019/12/20
     * @Description :  find this table page limit date , PageQuery{ paras:参数; orderBy:排序; pageNumber:页数; pageSize:每页记录数; totalPage:总页数; totalRow:总行数 }
     */
    public PageQuery<T> find_Page(PageQuery pageQuery);
}
