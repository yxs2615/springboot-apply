package com.project.utils.layui;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.Data;
import org.beetl.sql.core.engine.PageQuery;

import javax.validation.constraints.Min;
import java.io.Serializable;

/**
 * @program: jx
 * @description: layuI 传递分页参数工具类
 * @author: Mr.Yan
 * @create: 2018-09-22 15:16
 **/
@Data
public class PageUtils extends PageQuery implements Serializable {
    private static final long serialVersionUID = -1202716581589799959L;

    //  每页记录数
    @Min(value = 1, message = "当前页不能小于1")
    private int page = 1;

    //  当前页数
    @Min(value = 1, message = "当前页记录数不能小于1条")
    private int limit = 10;

    //  参数(前台用 JSON.stringify({"":"","",""}))
    private String params;

    /**
     * 分页
     *
     * @param pageSize 每页记录数
     * @param currPage 当前页数
     */
    public PageUtils(Integer page, Integer limit, String params) {
        if (ObjectUtil.isNotNull(page))
            this.page = page;
        if (ObjectUtil.isNotNull(limit))
            this.limit = limit;

        if (StrUtil.isNotBlank(params))
            this.params = params;

        //  利用@pageTag
        //  时设置当前页
        setPageNumber(this.page);
        //  设置记录数
        setPageSize(this.limit);
        if (ObjectUtil.isNotNull(this.params)) {
            //  设置参数
            JSONObject jsonObject = JSONUtil.parseObj(this.params);
            // Map paramMap = JSONObject.fromObject(params);
            setParas(jsonObject);
        }

    }


}
