package com.project.utils.layui;

import org.beetl.sql.core.engine.PageQuery;

import java.util.HashMap;

/**
 * @program: jx
 * @description: 返回 表格数据
 * @author: Mr.Yan
 * @create: 2018-09-22 15:19
 **/
public class LayDatas extends HashMap<String, Object> {

    // 表格
    public static LayDatas data(PageQuery pageQuery){
        LayDatas r = new LayDatas();
        r.put("code", 0);
        r.put("msg", "ok");
        r.put("count", pageQuery.getTotalRow());
        r.put("data", pageQuery.getList());
        return r;
    }

    // 树形表格
    public static LayDatas data(Object date){
        LayDatas r = new LayDatas();
        r.put("code", 0);
        r.put("msg", "ok");
        r.put("data", date);
        return r;
    }

}
