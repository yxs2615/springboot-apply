package com.project.utils;

import cn.hutool.core.util.StrUtil;
import com.project.bean.system.TblMenuBean;
import com.project.entity.system.menu.MenuEntity;
import com.project.entity.system.menu.MenuMetaEntity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author : Mr.Yan
 * @program : com.project.utils
 * @create : 2020/03/31
 * @description :
 */
public class MenuTreeUtil {

    private static MenuEntity beanToEntity(TblMenuBean menuBean){
        // 添加到菜单项中
        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setMenuId(menuBean.getMenuId());
        menuEntity.setName(StrUtil.isNotEmpty(menuBean.getComponentName()) ? menuBean.getComponentName() : menuBean.getMenuName()); // 设置名称
        // 一级目录需要加斜杠，不然会报警告
        menuEntity.setPath((menuBean.getParentMenuId().equals("0")) ? "/" + menuBean.getPath() : menuBean.getPath());
        menuEntity.setHidden((menuBean.getIsHidden().equals("Y")) ? true : false);  //  是否隐藏
        // 如果不是外链
        if (!(menuBean.getIsFrame().equals("Y")) ? true : false) {
            if (menuBean.getParentMenuId().equals("0")) {
                menuEntity.setComponent(StrUtil.isEmpty(menuBean.getComponent()) ? "Layout" : menuBean.getComponent());
            } else if (!StrUtil.isEmpty(menuBean.getComponent())) {
                menuEntity.setComponent(menuBean.getComponent());
            }
        }
        menuEntity.setMeta(new MenuMetaEntity(menuBean.getMenuName(), menuBean.getIcon(), !(menuBean.getCache().equals("Y")) ? true : false));
        return menuEntity;
    }

    /**
     * 根据父节点的ID获取所有子节点
     *
     * @param list 分类表
     * @return String
     */
    public static List<MenuEntity> getChildPerms(List<TblMenuBean> list, String parentMenuId) {
        List<MenuEntity> returnList = new ArrayList<MenuEntity>();
        for (Iterator<TblMenuBean> iterator = list.iterator(); iterator.hasNext(); ) {
            TblMenuBean menuBean = (TblMenuBean) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (menuBean.getParentMenuId().equals(parentMenuId)) {
                MenuEntity menuEntity = beanToEntity(menuBean); // 转换

                // 第一层
                recursionFn(list, menuBean , menuEntity);


                returnList.add(menuEntity);
            }
        }
        return returnList;
    }

    /**
     * 递归列表
     *
     * @param list
     * @param t
     */
    private static void recursionFn(List<TblMenuBean> list, TblMenuBean t ,MenuEntity menuEntity) {
        if (hasChild(list, t)) {
            menuEntity.setAlwaysShow(true);
            menuEntity.setRedirect(t.getRedirect());
            // 得到子节点列表
            List<TblMenuBean> childList = getChildList(list, t);
            t.setChildren(childList);

            List<MenuEntity> menuChildList = new ArrayList<>();
            for (TblMenuBean tChild : childList) {
                MenuEntity childEntity = beanToEntity(tChild);
                menuChildList.add(childEntity);
                menuEntity.setChildren(menuChildList);
                if (hasChild(list, tChild)) {
                    // 判断是否有子节点
                    Iterator<TblMenuBean> it = childList.iterator();
                    while (it.hasNext()) {
                        TblMenuBean n = (TblMenuBean) it.next();
                        recursionFn(list, n,childEntity);
                    }
                }
            }
        }else if(t.getParentMenuId().equals("0")){
            MenuEntity menuEntity2 = new MenuEntity();
            menuEntity2.setMeta(menuEntity.getMeta());
            // 非外链
            if (!(t.getIsFrame().equals("Y")) ? true : false) {
                menuEntity2.setPath(menuEntity.getPath());
                menuEntity2.setName(menuEntity.getName());
                menuEntity2.setComponent(menuEntity.getComponent());
            } else {
                menuEntity2.setPath(t.getPath());
            }
            menuEntity.setName(null);
            menuEntity.setMeta(null);
            menuEntity.setComponent("Layout");
            List<MenuEntity> list1 = new ArrayList<>();
            list1.add(menuEntity2);
            menuEntity.setChildren(list1);
        }
    }

    /**
     * 判断是否有子节点
     */
    private static boolean hasChild(List<TblMenuBean> list, TblMenuBean t) {
        return getChildList(list, t).size() > 0 ? true : false;
    }

    /**
     * 得到子节点列表
     */
    private static List<TblMenuBean> getChildList(List<TblMenuBean> list, TblMenuBean t) {
        List<TblMenuBean> tlist = new ArrayList<TblMenuBean>();
        Iterator<TblMenuBean> it = list.iterator();
        while (it.hasNext()) {
            TblMenuBean n = (TblMenuBean) it.next();
            if (n.getParentMenuId().equals(t.getMenuId())) {
                tlist.add(n);
            }
        }
        return tlist;
    }

}
