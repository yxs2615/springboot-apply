package com.project.bean.system;

import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

/**
 * @author : Mr.Yan
 * @program : com.project.bean
 * @create : 2020/03/29
 * @description :
 */
@Data
@Table(name = "tbl_role")
public class TblRoleBean {

    private String roleId;
    private String roleName;
    private Integer level;
    private String permission;
}
