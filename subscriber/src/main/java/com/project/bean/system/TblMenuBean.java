package com.project.bean.system;

import com.project.bean.BaseBean;
import com.project.bean.Insert;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author : Mr.Yan
 * @program : com.project.bean.system
 * @create : 2020/03/31
 * @description :
 */
@Data
@Table(name = "tbl_menu")
public class TblMenuBean extends BaseBean {

    public String menuId;
    @NotBlank(groups = {Insert.class}, message = "菜单名称为必填项")
    public String menuName;
    @NotBlank(groups = {Insert.class}, message = "上级菜单为必填项")
    public String parentMenuId;
    public String permission;
    public String isFrame;
    public String redirect;
    public String component;
    public String componentName;
    public String type;
    public String isHidden;
    public String sort;
    public String icon;
    public String path;
    public String cache;
    public List<TblMenuBean> children;

}
