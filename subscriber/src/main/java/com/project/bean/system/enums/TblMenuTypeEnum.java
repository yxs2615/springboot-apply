package com.project.bean.system.enums;

import lombok.Getter;

/**
 * @author : Mr.Yan
 * @program : com.project.bean.system.enums
 * @create : 2020/06/04
 * @description : 类型枚举
 */
@Getter
public enum  TblMenuTypeEnum {
    CATALOG("catalog", "目录"), // 目录
    BUTTON("button", "按钮"), //按钮
    MENU("menu", "菜单"); // 菜单

    public String type;

    public String name;

    private TblMenuTypeEnum(String type, String name){
        this.type = type;
        this.name = name;
    }
}
