package com.project.bean.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.project.bean.BaseBean;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * @author : Mr.Yan
 * @program : com.project.bean
 * @create : 2020/03/28
 * @description : 用户表对应数据库实体
 */
@Data
@Table(name = "tbl_user")
public class TblUserBean extends BaseBean {

    /** 用户唯一编码用于业务关联 */
    public String userId;
    /** 用户名 */
    public String userName;
    /** 密码 */
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-zA-Z])(.{8,20})$", message = "密码长度为8到20位,必须包含字母和数字，字母区分大小写")
    public String passWord;
    /** 性别 */
    public String sexId;
    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
    public Date birthday;
    /** 岗位 */
    public String jobId;
    /** 部门 */
    public String deptId;

}
