package com.project.bean.system;

import com.project.bean.BaseBean;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.List;

/**
 * @author : Mr.Yan
 * @program : com.project.bean.system
 * @create : 2020/03/29
 * @description : 部门实体
 */
@Data
@Table(name = "tbl_dept")
public class TblDeptBean extends BaseBean {
    public String deptId;
    public String deptName;
    public String parentDeptId;
    public List<TblDeptBean> children;
}
