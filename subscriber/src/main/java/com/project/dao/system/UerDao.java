package com.project.dao.system;

import com.project.bean.system.TblRoleBean;
import org.beetl.sql.core.annotatoin.SqlResource;

import java.util.List;
import java.util.Map;

/**
 * @author : Mr.Yan
 * @program : com.project.dao
 * @create : 2020/03/29
 * @description : 登陆接口
 */
@SqlResource(value = "system.userSql")
public interface UerDao {


    List<TblRoleBean> findRoleListByUserId(Map map);
}
