package com.project.dao.system;

import com.project.bean.system.TblMenuBean;
import com.project.entity.system.menu.MenuTreeEntity;
import org.beetl.sql.core.annotatoin.SqlResource;

import java.util.List;
import java.util.Map;

/**
 * @author : Mr.Yan
 * @program : com.project.dao.system
 * @create : 2020/03/31
 * @description :
 */
@SqlResource(value = "system.menuSql")
public interface MenuDao {
    List<MenuTreeEntity> findMenuList(Map<String, Object> map);

    List<TblMenuBean> build(TblMenuBean paramBean);
}
