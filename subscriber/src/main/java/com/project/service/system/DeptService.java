package com.project.service.system;

import com.project.bean.system.TblDeptBean;
import com.project.service.BaseService;

/**
 * @author : Mr.Yan
 * @program : com.project.service.system
 * @create : 2020/03/29
 * @description : 部门接口
 */
public interface DeptService extends BaseService<TblDeptBean> {
}
