package com.project.service.system;

import com.project.bean.system.TblMenuBean;
import com.project.entity.system.menu.MenuEntity;
import com.project.entity.system.menu.MenuTreeEntity;
import com.project.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * @author : Mr.Yan
 * @program : com.project.service.system
 * @create : 2020/03/31
 * @description :
 */
public interface MenuService extends BaseService<TblMenuBean> {

    /**
     * @author : Mr.Yan
     * @create : 2020/6/24
     * @description : 查询左侧得菜单和目录
     */
    List<TblMenuBean> build(TblMenuBean paramBean);
    /**
      * @author : Mr.Yan
      * @create : 2020/3/31
      * @description : 根据用户查询左侧菜单
      */
    List<MenuEntity> findMenuBuild(List<TblMenuBean> menuList);

    /**
      * @author : Mr.Yan
      * @create : 2020/6/5
      * @description : 查询菜单列表项
      */
    List<MenuTreeEntity> findMenuList(Map<String, Object> map);

    /**
      * @author : Mr.Yan
      * @create : 2020/6/7
      * @description : 根据当前级查询上级以及上上级的数据
      */
    List<MenuTreeEntity> findSuperior(TblMenuBean menuBean, List<MenuTreeEntity> menuBeanList);

}
