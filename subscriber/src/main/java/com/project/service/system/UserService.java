package com.project.service.system;

import com.project.bean.system.TblRoleBean;
import com.project.bean.system.TblUserBean;
import com.project.entity.system.user.UserInfoEntity;
import com.project.service.BaseService;

import java.util.List;

/**
 * @author : Mr.Yan
 * @program : com.project.service
 * @create : 2020/03/29
 * @description : 登陆service
 */

public interface UserService extends BaseService<TblUserBean> {



    /**
      * @author : Mr.Yan
      * @create : 2020/3/29
      * @description : 查询当前用户的角色
      */
    List<TblRoleBean> findRoleListByUserId(String userId);
}
