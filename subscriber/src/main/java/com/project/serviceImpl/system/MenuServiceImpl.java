package com.project.serviceImpl.system;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.project.bean.system.TblMenuBean;
import com.project.dao.system.MenuDao;
import com.project.entity.system.menu.MenuEntity;
import com.project.entity.system.menu.MenuTreeEntity;
import com.project.service.system.MenuService;
import com.project.serviceImpl.BaseServiceImpl;
import com.project.utils.MenuTreeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author : Mr.Yan
 * @program : com.project.serviceImpl.system
 * @create : 2020/03/31
 * @description :
 */
@Slf4j
@Service
@Transactional
public class MenuServiceImpl extends BaseServiceImpl<TblMenuBean> implements MenuService {

    @Autowired
    private MenuDao menuDao;

    @Override
    public List<TblMenuBean> build(TblMenuBean paramBean) {
       return menuDao.build(paramBean);
    }

    @Override
    public List<MenuEntity> findMenuBuild(List<TblMenuBean> menuList) {
        List<MenuEntity> childPerms = MenuTreeUtil.getChildPerms(menuList, "0");
        return childPerms;

    }

    @Override
    public List<MenuTreeEntity> findMenuList(Map<String, Object> map) {
        return menuDao.findMenuList(map);
    }

    @Override
    public  List<MenuTreeEntity> findSuperior(TblMenuBean menuBean, List<MenuTreeEntity> menuBeanList) {
        // 如果上级为空返回
        if (ObjectUtil.isNull(menuBean)) {
            return menuBeanList;
        }
        // 根据父id查询所有的上级菜单
        TblMenuBean tblMenuBean2 = new TblMenuBean();
        tblMenuBean2.setParentMenuId(menuBean.getParentMenuId());
        tblMenuBean2.setIsDelete(String.valueOf("N"));
        // 查询到当前的菜单列表项
        List<MenuTreeEntity> menuList = this.findMenuList(BeanUtil.beanToMap(tblMenuBean2));
        menuList.stream().forEach(menu -> {
            // 查到的集合添加到返回集 中
            menuBeanList.add(menu);
        });

        // 查询父级的选项
        TblMenuBean tblMenuBean = new TblMenuBean();
        tblMenuBean.setMenuId(menuBean.getParentMenuId());
        tblMenuBean.setIsDelete(String.valueOf("N"));
        return this.findSuperior(super.find_ObjByWhere(tblMenuBean), menuBeanList);
    }

}
