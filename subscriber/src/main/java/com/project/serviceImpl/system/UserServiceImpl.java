package com.project.serviceImpl.system;

import com.project.bean.system.TblRoleBean;
import com.project.bean.system.TblUserBean;
import com.project.dao.system.UerDao;
import com.project.service.system.UserService;
import com.project.serviceImpl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author : Mr.Yan
 * @program : com.project.serviceImpl
 * @create : 2020/03/29
 * @description : 登陆 impl
 */
@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<TblUserBean> implements UserService {

    @Autowired
    private UerDao userDao;

    @Override
    public List<TblRoleBean> findRoleListByUserId(String userId) {
        Map map = new HashMap();
        map.put("userId",userId);
        return userDao.findRoleListByUserId(map);
    }
}
