package com.project.serviceImpl.system;

import com.project.bean.system.TblDeptBean;
import com.project.service.system.DeptService;
import com.project.serviceImpl.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : Mr.Yan
 * @program : com.project.serviceImpl.system
 * @create : 2020/03/29
 * @description : 部门接口
 */
@Slf4j
@Service
@Transactional
public class DeptServiceImpl extends BaseServiceImpl<TblDeptBean> implements DeptService {
}
