package com.project.entity.system.menu;

import lombok.Data;

import java.io.Serializable;

@Data
public class MenuMetaEntity implements Serializable {

    private String title;

    private String icon;

    private Boolean noCache;

    public MenuMetaEntity(String title, String icon, Boolean noCache) {
        this.title = title;
        this.icon = icon;
        this.noCache = noCache;
    }
}
