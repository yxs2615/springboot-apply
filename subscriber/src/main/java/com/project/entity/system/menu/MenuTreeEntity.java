package com.project.entity.system.menu;

import com.project.bean.BaseBean;
import lombok.Data;

import java.util.List;

/**
 * @author : Mr.Yan
 * @program : com.project.control.system.menu.entity
 * @create : 2020/06/05
 * @description :
 */
@Data
public class MenuTreeEntity extends BaseBean {

    public String menuId;
    public String menuName;
    public String parentMenuId;
    public String permission;
    public String isFrame;
    public String redirect;
    public String component;
    public String componentName;
    public String type;
    public String isHidden;
    public String sort;
    public String icon;
    public String path;
    public String cache;
    public Boolean hasChildren;
    public List<MenuTreeEntity> children;

}
