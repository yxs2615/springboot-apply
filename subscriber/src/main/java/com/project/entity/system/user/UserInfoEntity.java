package com.project.entity.system.user;

import com.project.bean.system.TblRoleBean;
import com.project.bean.system.TblUserBean;
import lombok.Data;

import java.util.Set;

/**
 * @author : Mr.Yan
 * @program : com.project.control.login.entity
 * @create : 2020/03/29
 * @description : 用户角色
 */
@Data
public class UserInfoEntity extends TblUserBean{

    public Set<TblRoleBean> roles;


}
