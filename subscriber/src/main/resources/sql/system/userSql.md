findUserInfo
===
*@author : Mr.Yan
*@create : 2020/3/29
*@description : 登陆角色信息

    SELECT
       tbl_user.*,
       tbl.*
    FROM tbl_user
        LEFT JOIN
        ( 
            SELECT 
                userId, 
                GROUP_CONCAT( permission ) AS roles 
            FROM tbl_role 
            RIGHT JOIN tbl_user_role 
            ON tbl_role.roleId = tbl_user_role.roleId 
            WHERE userId = #userId# 
        ) AS tbl 
        ON tbl_user.userId = tbl.userId 
    WHERE tbl_user.userId = #userId# 
    
findRoleListByUserId
===
*@author : Mr.Yan
*@create : 2020/3/29
*@description : 当前用户的角色

    SELECT 
        tbl_role.* 
    FROM tbl_role
    RIGHT JOIN tbl_user_role ON tbl_role.roleId = tbl_user_role.roleId
    WHERE tbl_role.isValid = 'Y' AND isDelete = 'N' AND tbl_user_role.userId = #userId#
