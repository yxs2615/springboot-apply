build
===
*@author : Mr.Yan
*@create : 2020/6/24
*@description : 查询左侧得目录和菜单
    
    SELECT 
       TM.id,
       TM.menuId,
       TM.menuName,
       TM.parentMenuId,
       TM.permission,
       TM.isFrame,
       TM.component,
       TM.componentName,
       TM.type,
       TM.isHidden,
       TM.sort,
       TM.icon,
       TM.`cache`,
       TM.path,
       TM.sysCreateTime,
       TM.sysCreateUserId,
       TM.sysUpdateTime,
       TM.sysUpdateUserId,
       TM.isValid,
       TM.isDelete,
       TM.redirect
    FROM tbl_menu AS TM 
    WHERE 
       TM.isDelete = 'N' AND (TM.type = 'menu' OR TM.type = 'catalog')
    ORDER BY sort ASC
    

findMenuList
===
*@author : Mr.Yan
*@create : 2020/6/5
*@description : 查询菜单树形集合

    SELECT 
        TM.id,
        TM.menuId,
        TM.menuName,
        TM.parentMenuId,
        TM.permission,
        TM.isFrame,
        TM.component,
        TM.componentName,
        TM.type,
        TM.isHidden,
        TM.sort,
        TM.icon,
        TM.`cache`,
        TM.path,
        TM.sysCreateTime,
        TM.sysCreateUserId,
        TM.sysUpdateTime,
        TM.sysUpdateUserId,
        TM.isValid,
        TM.isDelete,
        TM.redirect,
        IF((SELECT count(id) FROM tbl_menu where parentMenuId = TM.menuId)>0 , true , false) AS hasChildren,
        (Select userName from tbl_user where userId = TM.sysCreateUserId ) AS sysCreateUserName,
        (Select userName from tbl_user where userId = TM.sysUpdateUserId ) AS sysUpdateUserName 
    FROM tbl_menu AS TM 
    WHERE 
        TM.isDelete = 'N'
        @if(isNotEmpty(menuName)){
            AND  TM.menuName like #'%' + menuName + '%'#
        @} else {
            @if(isNotEmpty(parentMenuId)){
                AND  TM.parentMenuId = #parentMenuId#
            @}
        @}
    ORDER BY sort ASC
