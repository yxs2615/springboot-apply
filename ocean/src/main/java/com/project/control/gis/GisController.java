package com.project.control.gis;

import cn.hutool.json.JSONUtil;
import com.project.bean.DicGisChildBean;
import com.project.bean.DicGisMainBean;
import com.project.control.BaseController;
import com.project.service.GisService;
import com.project.utils.result.ResultModel;
import com.project.utils.result.ResultTools;
import com.project.utils.token.CheckToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : Mr.Yan
 * @program : com.project.control.gis
 * @create : 2020/06/24
 * @description :
 */

@Slf4j
@RestController
@RequestMapping(value = "gis")
public class GisController extends BaseController {
    @Autowired
    private GisService gisService;

    /**
     * @author : Mr.Yan
     * @create : 2020/6/24
     * @description : 地图列表
     */
    @GetMapping(value = "findGis")
    @CheckToken
    public ResultModel findGis(@RequestParam String code) {
        log.info("地图管理 [gis/findGis] 接口");
        try {
            Map<String,Object> param = new HashMap<>();
            param.put("code",code);
            List<DicGisMainBean> dicGisMainBeanList = gisService.find_ListByWhere(param);
            log.info("结束");
            return ResultTools.success(dicGisMainBeanList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(String.format("结束：%s", e.getMessage()));
            return ResultTools.fail();
        }
    }

    /**
     * @author : Mr.Yan
     * @create : 2020/6/24
     * @description : 获取子类得地图
     */
    @GetMapping(value = "findGisChild")
    @CheckToken
    public ResultModel findGisChild(@RequestParam String tblName, @RequestParam String fcode) {
        log.info("地图管理 [dept/findGisChild] 接口");
        try {
            Map<String,Object> param = new HashMap<>();
            param.put("tblName",tblName);
            param.put("fcode",fcode);
            List<DicGisChildBean> gisChildBeanList = gisService.findGisChild(param);
            log.info("结束");
            return ResultTools.success(gisChildBeanList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(String.format("结束：%s", e.getMessage()));
            return ResultTools.fail();
        }
    }
    /**
     * @author : Mr.Yan
     * @create : 2020/6/24
     * @description : 获取子类得地图json
     */
    @GetMapping(value = "findGisJson")
    @CheckToken
    public ResultModel findGisJson(@RequestParam String code) {
        log.info("地图管理 [dept/findGisChild] 接口");
        try {
            String content = gisService.findGisChildJson(code);
            log.info("结束");
            return ResultTools.success(content);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(String.format("结束：%s", e.getMessage()));
            return ResultTools.fail();
        }
    }

}
