package com.project.control.ftp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author : Mr.Yan
 * @program : com.project.control.ftp
 * @create : 2020/07/13
 * @description : ftp配置文件
 */
@Data
@Component
@ConfigurationProperties(prefix = "ftp")
public class FtpProperties {

    /** ftp 地址 */
    private String host;
    /** ftp 端口 */
    private int port;
    /** ftp 用户名 */
    private String user;
    /** ftp 密码 */
    private String password;
    /** ftp 资源根路径 */
    private String remoteFolder;

}
