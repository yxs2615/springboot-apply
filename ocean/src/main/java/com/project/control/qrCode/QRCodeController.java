package com.project.control.qrCode;

import cn.hutool.core.util.ObjectUtil;
import com.project.control.BaseController;
import com.project.utils.qrCode.QRCodeUtil;
import com.project.utils.token.CheckToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author : Mr.Yan
 * @program : com.project.control.qrCode
 * @create : 2020/08/04
 * @description : 二维码
 */
@Slf4j
@RestController
@RequestMapping(value = "qrcode")
public class QRCodeController extends BaseController {

    @CheckToken(required = false)
    @GetMapping(value = "/createImage")
    public void createImage() throws IOException {
        OutputStream stream = null;
        try {
            stream = super.getResponse().getOutputStream();
            String content = "123";
            QRCodeUtil qrCodeUtil = new QRCodeUtil();
            qrCodeUtil.createQRImage(content, stream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ObjectUtil.isNotNull(stream)) {
                stream.flush();
                stream.close();
            }
        }
    }
}
