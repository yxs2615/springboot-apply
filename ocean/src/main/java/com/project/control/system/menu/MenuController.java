package com.project.control.system.menu;

import cn.hutool.core.date.DateUtil;
import com.project.bean.Insert;
import com.project.bean.Update;
import com.project.bean.system.TblMenuBean;
import com.project.control.BaseController;
import com.project.entity.system.menu.MenuEntity;
import com.project.entity.system.menu.MenuTreeEntity;
import com.project.service.system.MenuService;
import com.project.utils.MenuSuperiorUtil;
import com.project.utils.ShiroUtils;
import com.project.utils.annotation.AutoValid;
import com.project.utils.layui.PageUtils;
import com.project.utils.result.ResultModel;
import com.project.utils.result.ResultTools;
import com.project.utils.token.CheckToken;
import com.project.utils.uuid.UUIDEnum;
import com.project.utils.uuid.UUIDUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * @author : Mr.Yan
 * @program : com.project.control.system.menu
 * @create : 2020/03/31
 * @description : 菜单项
 */
@Slf4j
@RestController
@RequestMapping(value = "menu")
public class MenuController extends BaseController {

    @Autowired
    private MenuService menuService;

    /**
      * @author : Mr.Yan
      * @create : 2020/6/5
      * @description : 获取前端路由列表
      */
    @GetMapping(value = "build")
    @CheckToken
    public ResultModel build() {
        log.info("部门管理 [menu/build] 接口");
        try {
            TblMenuBean paramBean = new TblMenuBean();
            List<TblMenuBean> menuList = menuService.build(paramBean);
            List<MenuEntity> menuBuild = menuService.findMenuBuild(menuList);
            log.info("结束");
            return ResultTools.success(menuBuild);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(String.format("结束：%s", e.getMessage()));
            return ResultTools.fail();
        }
    }

    /**
      * @author : Mr.Yan
      * @create : 2020/6/5
      * @description : 菜单项分页
      */
    @GetMapping(value = "/list")
    @CheckToken
    public ResultModel list(PageUtils pageUtils){
        log.info("分页 [menu/list] 接口");
        List<MenuTreeEntity> menuList = menuService.findMenuList((Map<String,Object>)pageUtils.getParas());
        log.info("结束");
        return ResultTools.success(menuList);
    }

    /**
      * @author : Mr.Yan
      * @create : 2020/6/7
      * @description : 菜单查找父级
      */
    @GetMapping(value = "/superior")
    @CheckToken
    public ResultModel superior(@RequestParam String menuId){
        TblMenuBean tblMenuBean = new TblMenuBean();
        tblMenuBean.setMenuId(menuId);
        tblMenuBean.setIsDelete(String.valueOf("N"));

        // 根据菜单id 查询当前的的父级id 是什么
        TblMenuBean menuBean = menuService.find_ObjByWhere(tblMenuBean);
        // 查询上级以及上上级的所有数据
        List<MenuTreeEntity> superiorList = menuService.findSuperior(menuBean, new ArrayList<>());
        // 将数据整理成数据
        List<MenuTreeEntity> childPerms = MenuSuperiorUtil.getChildPerms(superiorList, "0");

        return ResultTools.success(childPerms);
    }
    /**
     * @author : Mr.Yan
     * @create : 2020/6/3
     * @description : 添加c菜单接口
     */
    @PostMapping(value = "/add")
    @CheckToken
    @AutoValid
    @RequiresRoles(value = {"admin", "putong"},  logical = Logical.OR)
    public ResultModel add(@RequestBody @Validated(Insert.class) TblMenuBean tblMenuBean , BindingResult result){
        log.info("添加 [user/add] 接口");
        try {
            tblMenuBean.setSysCreateTime(DateUtil.parse(DateUtil.now())); // 创建时间
            tblMenuBean.setSysCreateUserId(ShiroUtils.getPrincipal().getUserId()); // 创建人Id
            tblMenuBean.setMenuId(UUIDUtils.getUUID(UUIDEnum.MENU)); // 生成主键key
            int primaryKey = menuService.insert_Selective(tblMenuBean);
            if (primaryKey<=0) {
                return ResultTools.fail();
            }
            return ResultTools.success();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(String.format("结束：%s",e.getMessage()));
            return ResultTools.fail(e.getMessage());
        }
    }

    /**
     * @author : Mr.Yan
     * @create : 2020/6/3
     * @description : 更新菜单接口
     */
    @PutMapping(value = "/update")
    @CheckToken
    @AutoValid
    public ResultModel update(@RequestBody @Validated(Update.class) TblMenuBean tblMenuBean, BindingResult result){
        log.info("添加 [menu/update] 接口");
        try {
            tblMenuBean.setSysUpdateTime(DateUtil.parse(DateUtil.now())); // 更新时间
            tblMenuBean.setSysUpdateUserId(ShiroUtils.getPrincipal().getUserId()); // 更新人Id
            int primaryKey = menuService.update_Selective_ByPK(tblMenuBean);
            if (primaryKey<=0) {
                return ResultTools.fail();
            }
            return ResultTools.success();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(String.format("结束：%s",e.getMessage()));
            return ResultTools.fail(e.getMessage());
        }
    }

    /**
     * @author : Mr.Yan
     * @create : 2020/6/3
     * @description : 删除菜单接口
     */
    @DeleteMapping(value = "/delete")
    @CheckToken
    public ResultModel delete(@RequestBody List<Integer> pkList){
        log.info("添加 [menu/delete] 接口");
        try {
            List<TblMenuBean> menuBeanList = new ArrayList<>();
            for (Integer pk: pkList) {
                TblMenuBean menuBean = new TblMenuBean();
                menuBean.setId(pk);
                menuBean.setSysUpdateTime(DateUtil.parse(DateUtil.now())); // 更新时间
                menuBean.setSysUpdateUserId(ShiroUtils.getPrincipal().getUserId()); // 更新人Id
                menuBean.setIsDelete(String.valueOf("Y"));
                menuBeanList.add(menuBean);
            }
            int primaryKey = menuService.update_BatchSelective_ByPK(menuBeanList);
            if (primaryKey<=0) {
                return ResultTools.fail();
            }
            return ResultTools.success();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(String.format("结束：%s",e.getMessage()));
            return ResultTools.fail(e.getMessage());
        }
    }


}
