package com.project.control.system.dept;

import cn.hutool.json.JSONUtil;
import com.project.bean.system.TblDeptBean;
import com.project.control.BaseController;
import com.project.service.system.DeptService;
import com.project.utils.DeptTreeUtil;
import com.project.utils.result.ResultModel;
import com.project.utils.result.ResultTools;
import com.project.utils.token.CheckToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author : Mr.Yan
 * @program : com.project.control.system.dept
 * @create : 2020/03/29
 * @description : 部门管理
 */
@Slf4j
@RestController
@RequestMapping(value = "dept")
public class DeptController extends BaseController {

    @Autowired
    private DeptService deptService;

    /**
     * @author : Mr.Yan
     * @create : 2020/3/29
     * @description : 部门列表
     */
    @GetMapping(value = "list")
    @CheckToken
    public ResultModel list() {
        log.info("部门管理 [dept/list] 接口");
        try {
            TblDeptBean paramBean = new TblDeptBean();
            List<TblDeptBean> deptList = deptService.find_ListByWhere(paramBean);
            List<TblDeptBean> deptTreeList = DeptTreeUtil.getChildPerms(deptList, "20200330DEPT0001");
            log.info("结束");
            return ResultTools.success(JSONUtil.parseArray(deptTreeList));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(String.format("结束：%s", e.getMessage()));
            return ResultTools.fail();
        }
    }
}
