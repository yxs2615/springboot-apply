package com.project;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author : Mr.Yan
 * @program : com.project
 * @create : 2020/03/28
 * @description : 启动文件
 */
@Slf4j
@SpringBootApplication
public class OceanApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(OceanApplication.class, args);
        log.info("启动项目成功");
    }

}
