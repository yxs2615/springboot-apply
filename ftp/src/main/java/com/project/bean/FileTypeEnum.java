package com.project.bean;

import lombok.Getter;

/**
 * @author : Mr.Yan
 * @program : com.project.bean
 * @create : 2020/07/13
 * @description :
 */
@Getter
public enum FileTypeEnum {

    DIRECTORY("文件夹"),
    FIlE("文件");
    private String name;

    private FileTypeEnum (String name){
        this.name = name;
    }


}
