package com.project.bean;

import lombok.Data;

import java.util.Calendar;

/**
 * @author : Mr.Yan
 * @program : com.project.bean
 * @create : 2020/07/13
 * @description : ftpFileBean
 */
@Data
public class FtpFileBean {

    private String name; // 名称
    private boolean directory;
    private boolean file;
    private int processValue;
    private boolean showProcess;
    private long size; // 文件大小
    private Calendar timestamp; // 文件大小


}
