package com.project.service;

import cn.hutool.extra.ftp.Ftp;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.OutputStream;

/**
 * @author : Mr.Yan
 * @program : com.project.service
 * @create : 2020/07/13
 * @description : ftp 服务层
 */
public interface FtpService {

    /** 登陆ftp */
    Ftp ftpLogin (String host, int port, String user, String password);

    /** 获取ftp指定路径下的所有文件属性 {remoteFolder: 目标路径 (eg：/opt/resource)}*/
    FTPFile[] ftplsFiles (Ftp ftp, String  remoteFolder);

    /** 新建文件夹 { folderName: 文件夹名称，folderPath: 文件夹路径 } */
    String newFolder (Ftp ftp, String folderName, String folderPath);

    /** 删除文件 {name：文件名称, folderPath：文件根路径} */
    boolean deleteFile(Ftp ftp, String name, String folderPath);

    /** 删除文件夹 {name：文件名称, folderPath：文件根路径} */
    boolean deleteDir(Ftp ftp, String name, String folderPath);

    /** 关闭连接 */
     void ftpClose(Ftp ftp);

    /** 重命名 */
    boolean rename(Ftp ftp,String srcName, String toName, String s);

    /** 下载文件 {name：文件名称, folderPath：文件根路径} */
    void downloadFile(Ftp ftp, String name, String folderPath, HttpServletRequest request);

    /** 上传文件 {chooseFile：文件, folderPath：文件根路径}*/
    String updateFile(Ftp ftp, MultipartFile chooseFile, String folderPath);
}
