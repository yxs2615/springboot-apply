package com.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr.Yan
 * @program : PACKAGE_NAME
 * @create : 2020/09/02
 * @description :
 */
@SpringBootApplication
public class UploadApplication {

    public static void main(String [] arg0) {
            SpringApplication.run(UploadApplication.class, arg0);
    }
}
