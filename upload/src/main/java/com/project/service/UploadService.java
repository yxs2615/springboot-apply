package com.project.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * @author : Mr.Yan
 * @program : com.project.service
 * @create : 2020/09/02
 * @description : 上传文件接口
 */
public interface UploadService {

    /**
     * @author : Mr.Yan
     * @create : 2020/9/2
     * @description : 上传指定文件路径到指定文件地址,返回文件的名称
     */
    String uploadFile(MultipartFile file);

    /**
     * @author : Mr.Yan
     * @create : 2020/9/2
     * @description : 上传图片返回base64
     */
    String uploadImage(MultipartFile file);

}
