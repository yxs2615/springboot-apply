package com.project.control;

import com.project.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author : Mr.Yan
 * @program : com.project.control
 * @create : 2020/09/02
 * @description :
 */
@RestController
public class TestControl extends BaseController {

    @Resource
    private UploadService uploadService;

    @RequestMapping("/test")
    public void one () {
    }
}
