package com.project.utils.token;

import lombok.Data;

/**
 * @author : Mr.Yan
 * @program : com.project.utils.token
 * @create : 2020/03/29
 * @description : token
 */
@Data
public class TokenBean {

    public String token;

    public TokenBean (String token){
        this.token = token;
    }
}
