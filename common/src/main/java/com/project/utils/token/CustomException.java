package com.project.utils.token;

import com.project.utils.result.ResultModel;
import com.project.utils.result.ResultTools;

//或者继承RuntimeException（运行时异常）
public class CustomException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  ResultModel resultModel;

  // 提供无参数的构造方法
  public CustomException() {
  }

  // 提供一个有参数的构造方法，可自动生成
  public CustomException(String msg) {
    super(msg);// 把参数传递给Throwable的带String参数的构造方法
    this.resultModel = ResultTools.fail(401, msg);
  }

  public ResultModel getResultModel(){
    return this.resultModel;
  }

}
