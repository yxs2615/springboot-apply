package com.project.utils.result;

import lombok.Data;

@Data
public class ResultModel {

	private int status;// 返回码
	private String message;// 返回消息
	private Object data;// 数据源
}
