package com.project.utils.result;

/**
 * 请求结果处理类
 */
public class ResultTools {
    /****
     * 错误码记录：
     * 200--------成功
     * 400--------失败
     *
     */

    /**
     * @param status--返回码
     * @param message---404服务器内部异常时提示消息(返回码不是404时传空即可)
     * @param data------数据源
     * @return
     */
    private static ResultModel result(int status, String message, Object data) {
        ResultModel model = new ResultModel();
        model.setStatus(status);
        if (data != null) {
            model.setData(data);
        }
        switch (status) {
            case 200:
                model.setMessage("success!");
                break;
            case 400:
                if (!"".equals(message)) {
                    model.setMessage(message);
                } else {
                    model.setMessage("fail");
                }

                break;
            default:
                model.setMessage(message);
                break;
        }
        return model;
    }

    // success
    public static ResultModel success(Object data) {
        return result(200, "", data);
    }

    public static ResultModel success() {
        return result(200, "", null);
    }

    // fail
    public static ResultModel fail() {
        return result(400, "", null);
    }

    // fail
    public static ResultModel fail(String message) {
        return result(400, message, null);
    }

    // fail
    public static ResultModel fail(int status, String message) {
        return result(status, message, null);
    }

}
