package com.project.utils.qrCode;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 * @author : Mr.Yan
 * @program : com.project.utils.qrCode
 * @create : 2020/08/04
 * @description : 生成二维码工具类
 */
public class QRCodeUtil {

    //编码格式,采用utf-8
    private static final String UNICODE = "utf-8";
    //图片格式
    private static final String FORMAT = "JPG";
    //二维码宽度,单位：像素pixels
    private static final int QRCODE_WIDTH = 300;
    //二维码高度,单位：像素pixels
    private static final int QRCODE_HEIGHT = 300;
    //LOGO宽度,单位：像素pixels
    private static final int LOGO_WIDTH = 100;
    //LOGO高度,单位：像素pixels
    private static final int LOGO_HEIGHT = 100;
    //二维码配置
    private QrConfig qrConfig = null;

    /**
     * 构造函数
     */
    public QRCodeUtil(){
        this.qrConfig = new QrConfig();
        //返回指定的字符集CharSet
        Charset charset = Charset.forName(this.UNICODE);
        this.qrConfig.setCharset(charset)
                .setHeight(this.QRCODE_HEIGHT)
                .setWidth(this.QRCODE_WIDTH)
                .setImg(new File("C:\\Users\\YanSir\\Downloads\\setting.png"))
                .setErrorCorrection(ErrorCorrectionLevel.H);

    }

    public static void main (String  [] arg0) throws IOException {
       QRCodeUtil qrCodeUtil = new QRCodeUtil();
        OutputStream output = null;
        String content = "123";
       qrCodeUtil.createQRImage(content, output);
    }

    /**
     * @author : Mr.Yan
     * @create : 2020/8/4
     * @description : 文本生二维码输出本地
     */
    public void createQRImage(String content, File outfile){
        QrCodeUtil.generate(content, this.qrConfig, outfile);
    }

    /**
     * @author : Mr.Yan
     * @create : 2020/8/4
     * @description : 文本生成二维码, 输出流
     */
    public void createQRImage (String content, OutputStream output) throws IOException {
        BufferedImage image  = QrCodeUtil.generate(content, this.qrConfig);
        ImageIO.write(image, FORMAT, output);
    }

    /**
     * @author : Mr.Yan
     * @create : 2020/8/4
     * @description : 链接生成二维码
     */

    /**
     * @author : Mr.Yan
     * @create : 2020/8/4
     * @description : 文件生成二维码
     */
}
