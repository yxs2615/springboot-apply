package com.project.utils.uuid;

import lombok.Getter;

/**
 * @author : Mr.Yan
 * @program : com.project.bean.system.enums
 * @create : 2020/06/07
 * @description : 生成uuidkey
 */
@Getter
public enum UUIDEnum {
    MENU("MENU"),
    USER("USER");

    public String key;

    UUIDEnum(String key){
        this.key = key;
    }

}
