package com.project.utils.uuid;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.UUID;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author : Mr.Yan
 * @program : com.project.utils.uuid
 * @create : 2020/06/07
 * @description : 生成唯一值
 */
public class UUIDUtils {
    public static Set<String> si = new LinkedHashSet<>();
    public static void main(String [] arg0) throws InterruptedException {
        for (int i =0 ;i<1000;i++){
            si.add(getUUID(UUIDEnum.MENU));
        }
        for (int i =0 ;i<1000;i++){
            si.add(getUUID(UUIDEnum.MENU));
        }
        for (int i =0 ;i<1000;i++){
            si.add(getUUID(UUIDEnum.MENU));
        }

        Thread.sleep(3000);
        for (int i =0 ;i<1000;i++){
            si.add(getUUID(UUIDEnum.MENU));
        }
        for (int i =0 ;i<1000;i++){
            si.add(getUUID(UUIDEnum.MENU));
        }
        System.out.println(si.size());

    }
    public static String getUUID(UUIDEnum uuidEnum) {
        String[] chars = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G",
                "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

        StringBuffer shortBuffer = new StringBuffer();
        String uuid=UUID.randomUUID().toString().replace("-","");
        for (int j = 0; j < 6; j++) {
            String str = uuid.substring(j * 5, j * 5 + 7);
            int par = Integer.parseInt(str, 16);
            shortBuffer.append(chars[par % 36]);
        }
        // System.out.println(String.format("%s%s%s",DateUtil.format(new Date(),"YYYYMMdd"),key,shortBuffer.toString()));
        return String.format("%s%s%s",uuidEnum.getKey(), DateUtil.format(new Date(),"YYYYMMdd"), shortBuffer.toString());
    }

}
