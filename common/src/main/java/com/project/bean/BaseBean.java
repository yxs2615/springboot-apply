package com.project.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *  @Author: Yan
 *  @Date: 2020/3/26
 *  @Description: 公共实体bean
 */
@Data
public class BaseBean implements Serializable {

    /** 默认主键为id*/
    public Integer id;

    /** 电脑记录创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    public Date sysCreateTime;

    /** 电脑记录创建者唯一编码*/
    public String sysCreateUserId;

    /** 电脑记录创建者唯一名称*/
    public String sysCreateUserName;

    /** 电脑记录修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    public Date sysUpdateTime;

    /** 电脑记录修改者唯一编码*/
    public String sysUpdateUserId;

    /** 电脑记录修改者唯一名称*/
    public String sysUpdateUserName;

    /** 记录是否有效 Y(有效)/ N(无效)  */
    public String isValid = "Y";

    /** 记录是否删除 Y(删除) / N (未删除)*/
    public String isDelete = "N";

}
